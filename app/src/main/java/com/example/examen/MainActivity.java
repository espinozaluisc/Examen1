package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private Button btnEntrar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = findViewById(R.id.txtNombre);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String no=txtNombre.getText().toString();
                    if(no.matches("")){
                        Toast.makeText(MainActivity.this,"Falta capurar nombre", Toast.LENGTH_LONG).show();
                    }else {
                        Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                        //Enviar un dato String
                        intent.putExtra("nombre",no);
                        startActivity(intent);

                    }
            }
        });
    }
}
