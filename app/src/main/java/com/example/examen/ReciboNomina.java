package com.example.examen;

public class ReciboNomina {

    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina() {

    }

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal(){
        float res=0.0f;
        if(puesto==1){
            res=(horasTrabNormal*50)+(horasTrabExtras*100);
        }
        if(puesto==2){
            res=(horasTrabNormal*70)+(horasTrabExtras*140);
        }
        if(puesto==3){
            res=(horasTrabNormal*100)+(horasTrabExtras*200);

        }
        return res;
    }
    public float calcularImpuesto(){
        float res= calcularSubtotal()*.16f;
        return res;
    }
    public float calcularTotal(){
        float res=calcularSubtotal()-calcularImpuesto();
        return res;
    }
}
