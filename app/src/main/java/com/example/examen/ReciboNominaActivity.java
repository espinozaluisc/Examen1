package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText txtNumRecibo;
    private TextView txtNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtras;
    private RadioGroup radioGroup;
    private RadioButton rbn1;
    private RadioButton rbn2;
    private RadioButton rbn3;
    private TextView txtSubtotal;
    private TextView txtImpuesto;
    private TextView txtTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private ReciboNomina recibo = new ReciboNomina();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        radioGroup = findViewById(R.id.radioGroup);
        rbn1 = findViewById(R.id.rbn1);
        rbn2 = findViewById(R.id.rbn2);
        rbn3 = findViewById(R.id.rbn3);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotal = findViewById(R.id.txtTotal);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        String nombre = getIntent().getStringExtra("nombre");
        txtNombre.setText(nombre);
        recibo.setPuesto(1);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNumRecibo.getText().toString().matches("") || txtHorasNormal.getText().toString().matches("") || txtHorasExtras.getText().toString().matches("")) {
                    Toast.makeText(ReciboNominaActivity.this, "Falta capturar datos", Toast.LENGTH_LONG).show();
                } else {
                    recibo.setNombre(txtNombre.getText().toString());
                    recibo.setHorasTrabExtras(Float.parseFloat(txtHorasExtras.getText().toString()));
                    recibo.setHorasTrabNormal(Float.parseFloat(txtHorasNormal.getText().toString()));
                    recibo.setNumRecibo(Integer.parseInt(txtNumRecibo.getText().toString()));
                    txtSubtotal.setText(String.valueOf(recibo.calcularSubtotal()));
                    txtImpuesto.setText(String.valueOf(recibo.calcularImpuesto()));
                    recibo.setImpuestoPorc(recibo.calcularImpuesto());
                    txtTotal.setText(String.valueOf(recibo.calcularTotal()));
                }

            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasExtras.setText("");
                txtHorasNormal.setText("");
                txtImpuesto.setText("");
                txtSubtotal.setText("");
                txtNumRecibo.setText("");
                txtTotal.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasExtras.setText("");
                txtHorasNormal.setText("");
                txtImpuesto.setText("");
                txtSubtotal.setText("");
                txtNumRecibo.setText("");
                txtTotal.setText("");
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rbn1:
                        recibo.setPuesto(1);
                        break;
                    case R.id.rbn2:
                        recibo.setPuesto(2);
                        break;
                    case R.id.rbn3:
                        recibo.setPuesto(3);
                        break;
                }
            }
        });
    }
}
